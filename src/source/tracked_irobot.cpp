#include "tracked_irobot.h"

TrackedIRobot::TrackedIRobot(int id_Robot_in, int Robot_Type_in,
                             float last_passive_feedback_time_threshold_in,
                             float last_active_feedback_time_threshold_in,
                             float continuous_active_feedback_time_threshold_in,
                             float touch_down_timer_threshold_in,
                             float touch_down_distance_threshold_in)
{
    id_Robot   = id_Robot_in;
    Robot_Type = Robot_Type_in;
    last_passive_feedback_time_threshold      = last_passive_feedback_time_threshold_in;
    last_active_feedback_time_threshold       = last_active_feedback_time_threshold_in;
    continuous_active_feedback_time_threshold = continuous_active_feedback_time_threshold_in;
    touch_down_timer_threshold    = touch_down_timer_threshold_in;
    touch_down_distance_threshold = touch_down_distance_threshold_in;
    touch_down_requirement_timer.restart(false);
    is_inside_touch_down_distance = false;
}

void TrackedIRobot::getPosition(float &x_out, float &y_out)
{
    x_out = x;
    y_out = y;
    return;
}

void TrackedIRobot::updateRobotInformation(const droneMsgsROS::robotPoseStamped &irobot_info_update, bool is_new_irobot)
{
    if ( (!is_new_irobot) && (id_Robot != irobot_info_update.id_Robot) ) { //&& (Robot_Type != irobot_info_update.Robot_Type)) {
        // entering here is very bad misbehaveour by the IRobotFeedbackSupervisor
        return;
    }

    if ( (irobot_info_update.header.stamp - last_timestamp).toSec() > 0 ) { // new measurement is active
        last_timestamp = irobot_info_update.header.stamp;

        x = irobot_info_update.x;
        y = irobot_info_update.y;
        z = irobot_info_update.z;
        theta = irobot_info_update.theta;

        updateRobotType( irobot_info_update.Robot_Type, is_new_irobot);

        // ver lo del move_down_timer
        if ( sqrt(x*x + y*y) < touch_down_distance_threshold) {
            if (!is_inside_touch_down_distance) {
                is_inside_touch_down_distance = true;
                touch_down_requirement_timer.restart(false);
            }
        } else {
            is_inside_touch_down_distance = false;
        }
    }

    return;
}

void TrackedIRobot::updateRobotType( int new_Robot_Type, bool is_new_irobot ) {
    // Robot_Type  < 0, column
    // Robot_Type == 0, unknown
    // Robot_Type  > 0, target

    if ( is_new_irobot ) {
        Robot_Type = new_Robot_Type;
        if (new_Robot_Type > 0) { // target
            last_passive_feedback_time_threshold      = TRACKED_IROBOT_TARGET_last_passive_feedback_time_threshold;
            last_active_feedback_time_threshold       = TRACKED_IROBOT_TARGET_last_active_feedback_time_threshold;
            continuous_active_feedback_time_threshold = TRACKED_IROBOT_TARGET_continuous_active_feedback_time_threshold;
            touch_down_timer_threshold                = TRACKED_IROBOT_TARGET_touch_down_timer_threshold;
            touch_down_distance_threshold             = TRACKED_IROBOT_TARGET_touch_down_distance_threshold;
        } else { // Column or unknown
            last_passive_feedback_time_threshold      = TRACKED_IROBOT_COLUMN_last_passive_feedback_time_threshold;
            last_active_feedback_time_threshold       = TRACKED_IROBOT_COLUMN_last_active_feedback_time_threshold;
            continuous_active_feedback_time_threshold = TRACKED_IROBOT_COLUMN_continuous_active_feedback_time_threshold;
            touch_down_timer_threshold                = TRACKED_IROBOT_COLUMN_touch_down_timer_threshold;
            touch_down_distance_threshold             = TRACKED_IROBOT_COLUMN_touch_down_distance_threshold;
        }
        return;
    }

    if ( Robot_Type == new_Robot_Type ) {
        // old_Robot_Type == new_Robot_Type
    } else {
        // old_Robot_Type != new_Robot_Type
        if (new_Robot_Type  < 0) {
            Robot_Type = new_Robot_Type;
            last_passive_feedback_time_threshold      = TRACKED_IROBOT_COLUMN_last_passive_feedback_time_threshold;
            last_active_feedback_time_threshold       = TRACKED_IROBOT_COLUMN_last_active_feedback_time_threshold;
            continuous_active_feedback_time_threshold = TRACKED_IROBOT_COLUMN_continuous_active_feedback_time_threshold;
            touch_down_timer_threshold                = TRACKED_IROBOT_COLUMN_touch_down_timer_threshold;
            touch_down_distance_threshold             = TRACKED_IROBOT_COLUMN_touch_down_distance_threshold;
        } else {
            if ( (Robot_Type == 0) && (new_Robot_Type  > 0) ) {
                Robot_Type = new_Robot_Type;
                last_passive_feedback_time_threshold      = TRACKED_IROBOT_TARGET_last_passive_feedback_time_threshold;
                last_active_feedback_time_threshold       = TRACKED_IROBOT_TARGET_last_active_feedback_time_threshold;
                continuous_active_feedback_time_threshold = TRACKED_IROBOT_TARGET_continuous_active_feedback_time_threshold;
                touch_down_timer_threshold                = TRACKED_IROBOT_TARGET_touch_down_timer_threshold;
                touch_down_distance_threshold             = TRACKED_IROBOT_TARGET_touch_down_distance_threshold;
            }
        }
    }
}
