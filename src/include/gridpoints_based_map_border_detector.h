#ifndef GRIDPOINTS_BASED_MAP_BORDER_DETECTOR_H
#define GRIDPOINTS_BASED_MAP_BORDER_DETECTOR_H

#include "ros/ros.h"
#include "droneMsgsROS/points3DStamped.h"
#include "droneMsgsROS/vector3f.h"

#include <math.h>
#include "cvg_utils_library.h"

class GridpointsBasedMapBorderDetector
{
private:
    double    direction_yaw_deg;                // counter-clockwise as seen from above
    double    threshold_min_distance;           // m
    double    threshold_max_distance;           // m
    int       threshold_min_count;              // counts
    double    threshold_max_time;               // seconds
    double    last_valid_measurement_distance;  // m
    ros::Time last_valid_measurement_timestamp; // ros_timestamp

public:
    GridpointsBasedMapBorderDetector( double direction_yaw_deg_in, double threshold_min_distance_in,
                                      double threshold_max_distance_in, double threshold_max_time_in,
                                      int    threshold_min_count_in);
    GridpointsBasedMapBorderDetector();
    ~GridpointsBasedMapBorderDetector();

    void setConfigurationParameters( double direction_yaw_deg_in, double threshold_min_distance_in,
                                     double threshold_max_distance_in, double threshold_max_time_in,
                                     int    threshold_min_count_in);
    void calculateBorderDetection( const droneMsgsROS::points3DStamped::ConstPtr& msg );
    bool isBorderDetected();
    bool thisDirectionIsNotAllowed(double currently_advised_yaw_direction_deg , double GRBD_FORBIDDEN_ANGLE_RANGE_BORDER);
};

#endif // GRIDPOINTS_BASED_MAP_BORDER_DETECTOR_H
