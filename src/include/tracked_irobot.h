#ifndef TRACKED_IROBOT_H
#define TRACKED_IROBOT_H

#include "ros/ros.h"
#include "Timer.h"
#include "droneMsgsROS/robotPoseStamped.h"

#define TRACKED_IROBOT_TARGET_last_passive_feedback_time_threshold      ( 2.00 )
#define TRACKED_IROBOT_TARGET_last_active_feedback_time_threshold       ( 0.75 )
#define TRACKED_IROBOT_TARGET_continuous_active_feedback_time_threshold ( 1.50 )
#define TRACKED_IROBOT_TARGET_touch_down_timer_threshold                ( 3.00 )
#define TRACKED_IROBOT_TARGET_touch_down_distance_threshold             ( 0.50 )

#define TRACKED_IROBOT_COLUMN_last_passive_feedback_time_threshold      ( 1.00   ) // I don't know if this is necessary
#define TRACKED_IROBOT_COLUMN_last_active_feedback_time_threshold       ( 999.99 ) // not_necessary
#define TRACKED_IROBOT_COLUMN_continuous_active_feedback_time_threshold ( 999.99 ) // not_necessary
#define TRACKED_IROBOT_COLUMN_touch_down_timer_threshold                ( 999.99 ) // not_necessary
#define TRACKED_IROBOT_COLUMN_touch_down_distance_threshold             ( 999.99 ) // not_necessary

class TrackedIRobot
{
private: // see droneMsgsROS/msg/robotPose.msg
float     x, y, z;
float     theta;
int       id_Robot, Robot_Type;
ros::Time last_timestamp;

float last_passive_feedback_time_threshold;
float last_active_feedback_time_threshold;
float continuous_active_feedback_time_threshold;

Timer touch_down_requirement_timer;
float touch_down_timer_threshold;
float touch_down_distance_threshold;
bool  is_inside_touch_down_distance;

public:
    TrackedIRobot( int id_Robot_in, int Robot_Type_in,
                   float last_passive_feedback_time_threshold_in      = 3.00,
                   float last_active_feedback_time_threshold_in       = 0.75,
                   float continuous_active_feedback_time_threshold_in = 1.50,
                   float touch_down_timer_threshold_in                = 2.00,
                   float touch_down_distance_threshold_in             = 0.50);

    inline float timeSinceLastFeedBack()  { return (ros::Time::now() - last_timestamp).toSec(); }
    inline bool  isUnderPassiveFeedback() { return (timeSinceLastFeedBack() < last_passive_feedback_time_threshold); }
    inline bool  isUnderActiveFeedback()  { return (timeSinceLastFeedBack() < last_active_feedback_time_threshold ); }
    inline bool  isUnderContinuousActiveFeedback()  { return ( (timeSinceLastFeedBack()<continuous_active_feedback_time_threshold) && isUnderActiveFeedback() ); }
    inline bool  isTouchDownRequirement() { return ((touch_down_requirement_timer.getElapsedSeconds() < touch_down_timer_threshold) && is_inside_touch_down_distance); }

    inline int getIdRobot() { return id_Robot; }
    inline int getRobotType() { return Robot_Type; }

    void getPosition(float& x_out, float& y_out);

    void updateRobotInformation(const droneMsgsROS::robotPoseStamped &irobot_info_update, bool is_new_irobot);
    void updateRobotType( int new_Robot_Type, bool is_new_irobot = false );
};

#endif // TRACKED_IROBOT_H
