#ifndef DRONEIARC14MISSIONSCHEDULER_H
#define DRONEIARC14MISSIONSCHEDULER_H

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>
#include <sstream>

// DroneModule parent class
#include "droneModuleROS.h"

// droneMsgsROS

// Topic and Rosservice names
#include "communication_definition.h"

#include "IARC14MissionScheduler_statemachine.h"

#define FREQ_DRONEIARC14MISSIONSCHEDULER  30.0

class DroneIARC14MissionScheduler : public DroneModule {

// DroneModule related functions
public:
    DroneIARC14MissionScheduler();
    ~DroneIARC14MissionScheduler();
    void init();
    void open(ros::NodeHandle & nIn, std::string moduleName);
    void close();
    bool run();

public: // to allow to printw stuff
    DroneIARC14MissionSchedulerStateMachine iarc14_MS_state_machine;
};
#endif // DroneIARC14MissionScheduler_H
