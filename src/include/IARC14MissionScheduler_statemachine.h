#ifndef DRONEIARC14MISSIONSCHEDULERSTATEMACHINE_H
#define DRONEIARC14MISSIONSCHEDULERSTATEMACHINE_H

// [Task] [DONE] Define state machine states and initial state, add initial state to state machine
// [Task] [DONE] Add communication channels with the Mission Planner
// [Task] Add state sequencing

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include <sstream>

// State-machine stuff
#include "communication_definition.h"
#include "IARC14MissionScheduler_states.h"
#include "Timer.h"

// SetControlModeService for the controller, and position referece
#include "control/Controller_MidLevel_controlModes.h"

// external message processing
#include "speed_and_irobot_selector_disk.h"

// external/module interfaces
#include "drone_iarc14_brain_interface.h"
#include "dronetrajectorycontrollerinterface.h"
#include "droneekfstateestimatorinterface.h"

//Subscribers: iRobot robotPose...
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/robotPoseStamped.h"
#include "droneMsgsROS/robotPoseStampedVector.h"

#define MS_IN_FLIGHT_OBSTACLE_BOUNCING_SPEED    0.50    // m/s

class DroneIARC14MissionSchedulerStateMachine {
    ros::NodeHandle   n;

    // State-machine related variables
private:
    MissionSchedulerStates::StateType   current_state, next_state;
    int                      state_step;

public:
    DroneIARC14MissionSchedulerStateMachine();
    ~DroneIARC14MissionSchedulerStateMachine();
    void open(ros::NodeHandle & nIn);
    bool run();
private:
    bool processState();
    void stateTransitionCheck();
public:
    std::string getMissionScheduler_str();
    std::string getMissionSchedulerStateStep_str();

public: // printing purposes
    // external message processing
    SpeedAndIRobotSelectorDisk  speed_irobot_selector_disk;

private:
    // external/module interfaces
    DroneIARC14BrainInterface          iarc14_brain_interface;
    DroneTrajectoryControllerInterface controller_interface;
    DroneEKFStateEstimatorInterface    ekf_state_estimator_interface;

private:
    // process and parameter variables related to each Mission Scheduler state
//    case MissionSchedulerStates::INITIAL_STATE:
//    case MissionSchedulerStates::INIT_SEQUENCE:
//    case MissionSchedulerStates::HOVERING:
//    case MissionSchedulerStates::CONTROL_STARTUP_SEQUENCE:
//    case MissionSchedulerStates::IN_FLIGHT_BORDER_BOUNCING:
//    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_REFERENCE_SEQUENCE:
//    case MissionSchedulerStates::PBVS_TRACKER_IS_REFERENCE:
    Timer passive_VS_feedback_lost_timer;    // threshold of tracking lost event
//    case MissionSchedulerStates::TO_IN_FLIGHT_BORDER_BOUNCING_SEQUENCE:
//    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_FEEDBACK_SEQUENCE:
//    case MissionSchedulerStates::PBVS_TRACKER_IS_FEEDBACK:
    Timer active_VS_feedback_lost_timer;    // threshold of tracking lost event
//    case MissionSchedulerStates::TO_MOVE_TOWARDS_TOUCH_DOWN_SEQUENCE:
//    case MissionSchedulerStates::MOVE_TOWARDS_TOUCH_DOWN:
    // Timer active_VS_feedback_lost_timer;    // threshold of tracking lost event
//    case MissionSchedulerStates::TOUCH_DOWN_SEQUENCE:
    Timer touch_down_seq_timer;    // threshold of tracking lost event
//    case MissionSchedulerStates::FROM_TOUCH_DOWN_SEQUENCE:
//    default:
};

#endif // DRONEIARC14MISSIONSCHEDULERSTATEMACHINE_H
